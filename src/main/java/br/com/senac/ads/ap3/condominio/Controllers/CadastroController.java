/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Controllers;

import javax.validation.Payload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import br.com.senac.ads.ap3.condominio.Entities.Status;
import br.com.senac.ads.ap3.condominio.CrudRepositories.*;
import br.com.senac.ads.ap3.condominio.Entities.Usuario;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


/**
 *
 * @author frederico
 */
@CrossOrigin(origins = "https://condominio-webapp.herokuapp.com/")
@RestController
public class CadastroController {
    @Autowired
    private UsuarioRepository usuarioRepository;
   
   
    @PostMapping(path="/usuarios")
    public Usuario cadastrarUsuario(@RequestParam("nome") String nome,     
                                    @RequestParam("email") String email, 
                                    @RequestParam("cpf") String cpf,
                                    @RequestParam("senha") String senha){
        
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setSenha(senha);
        usuario.setCpf(cpf);
        usuario.setEmail(email);
        
        try {
            
            usuarioRepository.save(usuario);
            usuarioRepository.flush();
            return usuario;
            
        } catch (Exception e) {
            
            return new Usuario();
            
        }
                  
    } 

 
    @GetMapping(path="/usuarios")
    public List<Usuario> listarDadosUsuarios(){
        
        return usuarioRepository.findAll();
        
    };
    
    @GetMapping(path="/usuarios/{id}")
    public Usuario listarDadosUsuarios(@PathVariable("id") int id){
        
        return usuarioRepository.findById(id).get();
        
    }
            
    @PutMapping(path="/usuarios/{id}")
    public Usuario atualizarUsuario(@PathVariable("id") int id, 
                                    @RequestParam("nome") String nome,     
                                    @RequestParam("email") String email, 
                                    @RequestParam("cpf") String cpf,
                                    @RequestParam("senha") String senha){
    
            try {
            // 
            
            Usuario usuario = usuarioRepository.findById(id).get();
            
            if (!usuario.getNome().equals(nome)){
                usuario.setNome(nome);
            };
            
            if (!usuario.getCpf().equals(cpf)){
                usuario.setSenha(senha);
            };
            
            if (!usuario.getEmail().equals(email)){
                usuario.setEmail(email);
            };
            
            if (!usuario.getSenha().equals(senha)){
                usuario.setSenha(senha);
            };
            
            usuarioRepository.save(usuario);
            
            return usuario;
            
            
        } catch (Exception e) {
            
            return new Usuario();
            
        }    
    
    }
            
    @DeleteMapping(path="/usuarios/{id}")
    public void removerUsuario(@PathVariable int id){
    
        usuarioRepository.deleteById(id);
    
    }
}



