/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author frederico
 */

@Entity(name="Condominio")
@Table(name="condominios")
public class Condominio {
    
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")    
    private int id;
    
    @Column(name = "nome")
    private String nome;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "condominio_moradores",
    joinColumns = @JoinColumn(name = "condominio_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "morador_id", referencedColumnName = "id"))
    private List<Morador> moradores;
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Morador> getMoradores() {
        return this.moradores;
    }

    public void addMorador(Morador moradores) {
        
        this.moradores.add(moradores);
    }
    
    public void removeMorador(Morador morador){
        
        moradores.remove(morador);
        
    }
}
