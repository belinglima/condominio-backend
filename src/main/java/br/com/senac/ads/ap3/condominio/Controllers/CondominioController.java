/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Controllers;

import br.com.senac.ads.ap3.condominio.CrudRepositories.CondominioRepository;
import br.com.senac.ads.ap3.condominio.CrudRepositories.UsuarioRepository;
import br.com.senac.ads.ap3.condominio.Entities.Condominio;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author fredericokremer
 */



@RestController
public class CondominioController {

    @Autowired
    private CondominioRepository condominioRepository;
    
    @PostMapping("/condominios")
    public Condominio cadastrarCondominios (@Valid @RequestBody Condominio condominio){
        
        try {
            
            condominioRepository.save(condominio);
            condominioRepository.flush();
            return condominio;
            
        } catch (Exception exception) {
            
            return new Condominio();
        
        }
        
    };
    
    
    
    @GetMapping("/condominios")
    public List<Condominio> listarDadosComdominio(){
        
        List<Condominio> condominios = condominioRepository.findAll();
        return condominios;        
        
    };
    
    @GetMapping("/condominios/{condominioId}")
    public Condominio listarDadosCondominio(@PathVariable("condominioId") int condominioId){
        
        try {
            
            return condominioRepository.findById(condominioId).get();
            
        } catch (Exception exception){
            
            return new Condominio();
            
        }
        
    };
    
    @PutMapping("/condominios/{id}")
    public Condominio atualizarDadosCondominio(@PathVariable("condominioId") int condominioId,
                                               @RequestParam("nome") String nome){
        
        Condominio condominio = condominioRepository.findById(condominioId).get();
        condominio.setNome(nome);
        
        condominioRepository.save(condominio);
        condominioRepository.flush();
        
        return condominio;
        
    };
    
    @DeleteMapping("/condominios/{id}")
    public void removerCondominio(@PathVariable("condominioId") int condominioId){
        
        condominioRepository.deleteById(condominioId);
          
    };
    
}
