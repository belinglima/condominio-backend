/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Controllers;

import br.com.senac.ads.ap3.condominio.CrudRepositories.CondominioRepository;
import br.com.senac.ads.ap3.condominio.CrudRepositories.MoradorRepository;
import br.com.senac.ads.ap3.condominio.Entities.Condominio;
import br.com.senac.ads.ap3.condominio.Entities.Morador;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author frederico
 */

@RestController
public class MoradorController {
    
    private CondominioRepository condominioRepository;
    private MoradorRepository moradorRepository;
    
    @PostMapping("/condominios/{condominio_id}/moradores")
    public Morador CadastrarMorador (@PathVariable("condominio_id") int condominioId, 
                             @RequestParam("usuario_id") int usuarioId,
                             @RequestParam("apartamento") String apartamento,
                             @RequestParam("bloco") String bloco) {
        
        Condominio condominio = condominioRepository.findById(condominioId).get();
        
        Morador morador = new Morador();
        morador.setUsuario_id(usuarioId);
        morador.setBloco(bloco);
        morador.setApartamento(apartamento);
        
        moradorRepository.save(morador);
        moradorRepository.flush();
        
        condominio.addMorador(morador);
        condominioRepository.save(condominio);
        condominioRepository.flush();
        
        return morador;
        
    }
    
    @GetMapping("/condominios/{condominio_id}/moradores")
    public List<Morador> recuperarDadosMoradores(@PathVariable("condominio_id") int condominioId){
        
        Condominio condominio = condominioRepository.findById(condominioId).get();
        return condominio.getMoradores();
        
    }
    
    @GetMapping("/condominios/{condominio_id}/moradores/{id}")
    public Morador recuperarDadosMorador(@PathVariable("id") int moradorId){
        
        try {
            
            Morador morador = moradorRepository.findById(moradorId).get();
            return morador;
            
        } catch (Exception exception) {
            
            return new Morador();
            
        }
     
        
    }
    
    @PutMapping("/condominios/{condominio_id}/moradores/{id}")
    public Morador atualizarDadosMorador(@PathVariable("id") int moradorId,
                                         @RequestParam("bloco") String bloco,
                                         @RequestParam("apartamento") String apartamento){
        
        Morador morador = moradorRepository.findById(moradorId).get();
        morador.setApartamento(apartamento);
        morador.setBloco(bloco);
        moradorRepository.save(morador);
        moradorRepository.flush();
        
        return morador;
        
    }
    
    
    @DeleteMapping("/condominios/{condominio_id}/moradores/{id}")
    public void removerMorador(@PathVariable("id") int moradorId){
        
        
        Morador morador = moradorRepository.findById(moradorId).get();
        moradorRepository.delete(morador);
        
    }
    
    
    
}
