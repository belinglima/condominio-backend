/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Entities;

import br.com.senac.ads.ap3.condominio.CrudRepositories.UsuarioRepository;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.security.*;
import java.math.*;
/**
 *
 * @author fredericokremer
 */

@Entity(name="Sessao")
@Table(name="sessoes")
public class Sessao {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "sessao_hash")
    private String sessaoHash;
    private long usuario_id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="usuario_id", nullable=false, insertable=false, updatable=false)
    private Usuario usuario;
    @Column(name = "administrador")
    private boolean administrador;
    @Column(name = "status")
    private boolean status;
    
    public Sessao(){
        
    }
    
    public Sessao(long usuarioId) throws NoSuchAlgorithmException {
    	
        String hashSeed = LocalDateTime.now().toString() + 
                          String.valueOf(usuarioId);
        
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, messageDigest.digest(hashSeed.getBytes()));
        this.sessaoHash = hash.toString(16);
        
    	this.usuario_id = usuarioId;
        this.status = true;
        this.administrador = false;
           
    }
    
    public Sessao(long usuarioId, boolean admin) throws NoSuchAlgorithmException {
    	
        String hashSeed = LocalDateTime.now().toString() + 
                          String.valueOf(usuarioId);
        
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, messageDigest.digest(hashSeed.getBytes()));
        this.sessaoHash = hash.toString(16);
        
    	this.usuario_id = usuarioId;
        this.status = true;
        this.administrador = admin;
           
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getUsuario_id() {
        return usuario_id;
    }
    
    public void setUsuario(Usuario usuario){
        this.usuario = usuario;
    }

    public void setUsuario_id(long usuario_id) {
        this.usuario_id = usuario_id;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public String getSessaoHash(){
        
        return this.sessaoHash;
        
    }
    
    public void setSessaoHash(String sessaoHash){
        
        this.sessaoHash = sessaoHash;
        
    }
    
    
    
}
