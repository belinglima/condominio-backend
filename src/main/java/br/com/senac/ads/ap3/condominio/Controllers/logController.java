package br.com.senac.ads.ap3.condominio.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.senac.ads.ap3.condominio.CrudRepositories.SessaoRepository;
import br.com.senac.ads.ap3.condominio.CrudRepositories.UsuarioRepository;
import br.com.senac.ads.ap3.condominio.Entities.Login;
import br.com.senac.ads.ap3.condominio.Entities.Sessao;
import br.com.senac.ads.ap3.condominio.Entities.Usuario;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;


@CrossOrigin(origins = "https://condominio-webapp.herokuapp.com/")
@RestController
public class logController {
	
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private SessaoRepository sessaoRepository;
    

	@PostMapping("/login")
	public Sessao login(@Valid @RequestBody Login login) throws NoSuchAlgorithmException {
		
		List<Usuario> usuarios = usuarioRepository.findAll();
		
		for(int i = 0; i < usuarios.size(); i++) {
			
			Usuario usuario = usuarios.get(i);
			if (usuario.getEmail().equals(login.getEmail()) && usuario.getSenha().equals(login.getSenha())) {
				Sessao sessao = new Sessao(usuario.getId());
				sessaoRepository.save(sessao);
				sessaoRepository.flush();
				return sessao;
				
			}
			
		};
		
		return new Sessao();
		
	};
        
        @PutMapping("/logout")
        public Sessao logout(@RequestParam("hash") String sessaoHash){
            
            List<Sessao> sessoes = sessaoRepository.findAll();
            for (int i = 0; i < sessoes.size(); i++){
                
                Sessao sessao = sessoes.get(i);
                if (sessao.getSessaoHash().equals(sessaoHash) &&
                    sessao.getStatus() == true){
                    
                    sessao.setStatus(false);
                    sessaoRepository.save(sessao);
                    sessaoRepository.flush();
                    
                    
                }
                
            }
            
            Sessao sessao = new Sessao();
            sessao.setStatus(false);
            
            return sessao;
            
        };
        
        
        @GetMapping("/logcheck")
        public Sessao logcheck(@RequestParam("hash") String sessaoHash){
            
            List<Sessao> sessoes = sessaoRepository.findAll();
            for (int i = 0; i < sessoes.size(); i++){
                
                Sessao sessao = sessoes.get(i);
                if (sessao.getSessaoHash().equals(sessaoHash) &&
                    sessao.getStatus() == true){
                    
                    return sessao;
                    
                }
                
            }
            
            Sessao sessao = new Sessao();
            sessao.setStatus(false);
            
            return sessao;
            
        };
       
       
	

}
