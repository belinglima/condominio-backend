/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.CrudRepositories;

import br.com.senac.ads.ap3.condominio.Entities.Sessao;
import br.com.senac.ads.ap3.condominio.Entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author fredericokremer
 */
public interface SessaoRepository extends JpaRepository<Sessao, Integer>{
    
    
    
}
