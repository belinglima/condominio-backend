/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ads.ap3.condominio.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author frederico
 */

@Entity(name="Moradores")
@Table(name="moradores")
public class Morador {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")    
    private int id;
    @Column(name = "usuario_id")
    private int usuario_id;
    @Column(name = "bloco")
    private String bloco;
    @Column(name = "apartamento")
    private String apartamento;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "condominio_moradores",
    joinColumns = @JoinColumn(name = "morador_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "condominio_id", referencedColumnName = "id"))
    @JsonIgnore
    private Condominio condominio;

    public Morador(int usuario_id, String bloco, String apartamento) {
        this.usuario_id = usuario_id;
        this.bloco = bloco;
        this.apartamento = apartamento;
    }

    public Morador() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getBloco() {
        return bloco;
    }

    public void setBloco(String bloco) {
        this.bloco = bloco;
    }

    public String getApartamento() {
        return apartamento;
    }

    public void setApartamento(String apartamento) {
        this.apartamento = apartamento;
    }
    
    
    
}
