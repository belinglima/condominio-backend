package br.com.senac.ads.ap3.condominio.Entities;

public class Status {
    
    private String complement;
    private String message;
    
    public Status(){
        
    }

    public Status(String message) {
        this.message = message;
    }
    
    public Status(String message, String complement) {
        this.message = message;
        this.complement = complement;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }
    
    
    
}
