/** DELETAR ESQUEMA, CASO ELE EXISTA **/

DROP TABLE IF EXISTS usuarios;
DROP TABLE IF EXISTS enderecos;
DROP TABLE IF EXISTS sessoes;
DROP TABLE IF EXISTS condominios;
DROP TABLE IF EXISTS moradores;
DROP TABLE IF EXISTS condominios_moradores;
DROP TABLE IF EXISTS mensagens;
DROP TABLE IF EXISTS mensagens_destinatarios;

CREATE TABLE usuarios (
    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(100) DEFAULT NULL,
    sobrenome VARCHAR(100) DEFAULT NULL,
    cpf VARCHAR(20) NOT NULL UNIQUE,
    email VARCHAR(100) NOT NULL UNIQUE,
    senha VARCHAR(100) NOT NULL,
    administrador BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE sessoes (

    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    usuario_id INTEGER,
    sessao_hash VARCHAR(50),   
    administrador BOOLEAN,
    status BOOLEAN
    
);

CREATE TABLE condominios (

    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(100),
    logradouro VARCHAR(100) DEFAULT NULL,
    cep VARCHAR(100) DEFAULT NULL,
    numero VARCHAR(100) DEFAULT NULL,
    complemento VARCHAR(100) DEFAULT NULL,
    bairro VARCHAR(100) DEFAULT NULL,
    cidade VARCHAR(100) DEFAULT NULL,
    uf VARCHAR(5) DEFAULT NULL

);

CREATE TABLE moradores (

    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    usuario_id INTEGER,
    apartamento VARCHAR(50),
    bloco VARCHAR(10)

);

CREATE TABLE condominio_moradores (

    morador_id INTEGER,
    condominio_id INTEGER
    
);

CREATE TABLE mensagens (

    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    remetente INTEGER,
    titulo BLOB,
    data_de_criacao DATE

);

CREATE TABLE mensagens_destinatarios (

    mensagem_id INTEGER,
    destinatario_id INTEGER

);

/** INSERIR DADOS **/


