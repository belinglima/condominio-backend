/** DELETAR ESQUEMA, CASO ELE EXISTA **/


INSERT INTO usuarios (nome, cpf, email, senha, administrador) VALUES 

    ('admin',     '12345678900', 'admin@condominio.com.br',     'admin', true),
    ('frederico', '03133193919', 'frederico@condominio.com.br', '1234',  false),
    ('marciel',   '91313213131', 'marciel@condominio.com.br',   '1234',  false),
    ('salomão',   '13131312312', 'salomao@condominio.com.br',   '1234',  false);

INSERT INTO condominios (nome, logradouro, cep, numero, complemento, bairro, cidade, uf) VALUES 
    ('Almirante Barroso', 'Almirante Barroso', '12312312', '132', 'rua sem saida',            'centro', 'Pelotas', 'RS'),
    ('Marechal Deodoro',  'Marechal Deodoro',  '12312312', '551', 'do lado do tigrão frangos','centro', 'Pelotas', 'RS');


INSERT INTO moradores (usuario_id, apartamento, bloco) VALUES 
    (2, '601',  'B'), 
    (3, '205',  ''),
    (4, '102A', 'A');


INSERT INTO condominio_moradores (morador_id, condominio_id) VALUES 
    (1,1), 
    (2,2),
    (3,1);


