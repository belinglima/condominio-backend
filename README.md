# Condomin.io: um *webApp* para seus condomínio

## O que é?

Condomin.io é uma webApp desenvolvido para facilitar a comunicação entre moradores de um condominio.

## Tecnologias 

**Linguagem / Framework (back-end):** Java  / Spring.
**Linguagem / Framework (front-end):** HTML, Javascript & CSS  / Bootstrap & Angular.
**SGDBR:** MySQL (via JawsDB)
**DevOps:** Hospedado na plataforma Heroku


## Repositórios

**Front-end:** [https://github.com/fredericokremer/condominio-frontend/](https://github.com/fredericokremer/condominio-frontend/)
**Back-end:**  [https://github.com/fredericokremer/condominio-backend/](https://github.com/fredericokremer/condominio-backend/)

## Licensa

O projeto utiliza a licença MIT visto que esta garante maiores liberdades ao usuário quanto
à distribuição do código e seu uso, sem comparada à licença GPL.

## Requisitos

### Requisitos Funcionais (RF)

`RF00001`: Cadastros de usuário.

`RF00002`: Login.

`RF00003`: Registro de moradores.

`RF00004`: Envio de mensagens.

### Requisitos Não-Funcionais (RNF)

`RNF00001`: Interface *web*.

`RNF00002`: Sistema Responsivo.

## Diagrama Entidade-Relacionamento (ER)

![Diagrama ER](ER/diagrama-novo.png)

## Rotas REST

O sistema utiliza uma arquitetura REST, sendo utilizadas as seguintes rotas para o acesso aos dados:

### Cadastro e Acesso

`[POST] /usuario/` :white_check_mark:

`[GET] /usuario/` :white_check_mark:

`[GET] /usuario/{id}` :white_check_mark:

`[PUT] /usuario/{id}` :white_check_mark:

`[DELETE] /usuario/{id}` :white_check_mark:

### Cadastro de condominios (pelo Admin)

`[POST] /condominio/`

`[GET] /condominio/`

`[GET] /condominio/{id}`

`[PUT] /condominio/{id}`

`[DELETE] /condominio/{id}`

### Cadastro de moradores (pelo Admin)

`[POST] /morador/`

`[GET] /morador/`

`[GET] /morador/{id}`

`[DELETE] /morador/{id}`

### Envio de mensagens (pelo Admin)

`[POST] /mensagem/`

`[GET] /mensagem/`

`[GET] /mensagem/{id}`

`[DELETE] /mensagem/{id}`

